/* ========================================================================= *
 *                                                                           *
 *     GENETIC ALGORHITHM + MPI DEMO                                         *
 *                                                                           *
 *     2013 Maxim Novikov                                                    *
 *                                                                           *
 *     This code demonstrates the genetic algorhythm                         *
 *     for global optimisation.                                              *
 *                                                                           *
 *     Warning: This code is a part of superprak                             *
 *     assignment. It may be bad-written, it may misbehave                   *
 *     but who cares?                                                        *
 *     THIS CODE COMES WITH ABSOLUTLEY NO WARRANTY                           *
 *                                                                           *
 * ========================================================================= */
 
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <mpi.h>
#include <time.h>   
#include <omp.h>

#define debug_mode 0

/* ========================================================================= *
 * =                                             PARAMETERS FOR THIS DEMO  = *
 * ========================================================================= */

// Numder of dimentions
#define use_n_dims 400

/* ========================================================================= *
 * =                                                MPI-RELATED VARIABLES  = *
 * ========================================================================= */

int process_rank; // MPI-id number for this process
int number_of_processes; // Total number of processes

  
  

/* ========================================================================= *
 * =                                   DEFAULT PARAMETERS FOR GENETIC ALG  = *
 * ========================================================================= */

// How many spicies will be in population by default
#define default_population_size 1000  

/* ========================================================================= *
 * =                                             FUNCTION TO BE OPTIMISED  = *
 * ========================================================================= */
 
double loss(std::vector<bool> &V){
	double S = 0;
	#pragma omp parallel for reduction (+:S)
	for (unsigned int i=0; i < V.size(); ++i)
		S += V[i];	

	return S;
}


/* ========================================================================= *
 * =                                            POPULATION INITIALISATION  = *
 * ========================================================================= */

std::vector< std::vector<bool> > init_population(
	int population_size, int n_dims){
	
	std::vector< std::vector<bool> > population(population_size);
	std::vector<bool> V(n_dims);
	for (int i = 0; i < population_size;++i){
		population[i] = V;
		for (int j=0; j<n_dims;++j)
			population[i][j] = rand()%2;
	}
	return population;
}



/* ========================================================================= *
 * =                                    CROSSOVER, MUTATION AND SELECTION  = *
 * ========================================================================= */

void	population_rand_permute(std::vector< std::vector<bool> > &population){
	for(int i=0;i<population.size();++i){
			int p = rand()%population.size();
			std::vector<bool> v = population[i];
			population[i] = population[p];
			population[p] = v;
		}

} 

void evolution_step(
	double (*F)(std::vector<bool> &),
	std::vector< std::vector<bool> > &population) {
	
	#if debug_mode
	std::cout << "Entering evolution_step. processes:"<< number_of_processes << std::endl;
	#endif
	
	// random permutation of the population
	// is used to make sure any pair is possible
	population_rand_permute(population); 
	
	
	int n_spicies = population.size();
	int n_workers = number_of_processes - 1; // 0 process is mastert
 // how many spicies goes to each process
	int n_spicies_per_process = n_spicies/(n_workers);
	int n_spicies_for_last = n_spicies - (n_workers-1)*n_spicies_per_process; 
	int n_dims = population[0].size(); // dimensionality of the data
	
	double *msg   = new double[2*n_spicies_per_process*n_dims+2];
	double *msg_last   = new double[2*n_spicies_for_last*n_dims+2];
	
	
	#if debug_mode
	std::cout << "Sending data to workers" << std::endl;
	#endif
	//send each slave process it's part of data
	for (int i=1;i<number_of_processes;++i){ 
		double *d = (i==(number_of_processes-1)) ? msg : msg_last;
		int cur_n = (i==(number_of_processes-1)) ? 
								n_spicies_per_process:n_spicies_for_last;
		int start = (i-1)*n_spicies_per_process;
		int offset=2;
		d[0] = n_dims;
		d[1] = cur_n;
		for(int j=0;j<cur_n;++j)
			for(int p=0;p<n_dims;++p)
				d[offset++] =population[start+j][p]; 
		
		#if debug_mode
		std::cout << "Sending "<<  cur_n*n_dims+2 << "values\n";
		#endif
		MPI_Send(d, cur_n*n_dims+2, MPI_DOUBLE, i, 0, MPI_COMM_WORLD);

	}
	
	
	#if debug_mode
	std::cout << "Collectiong data from workers" << std::endl;
	std::cout << "Waiting for "<< 2*n_spicies_for_last*n_dims << "values maximum\n";
	#endif
	//collect results from all slave processes
	std::vector<std::vector<std::vector<bool> > > subpopulations(n_workers);
	std::vector<int> positions(n_workers,0);
	
	for(int np=0;np<n_workers;++np){
		MPI_Status status;
		MPI_Recv(msg_last, 2*n_spicies_for_last*n_dims, MPI_DOUBLE, MPI_ANY_SOURCE,
					MPI_ANY_TAG, MPI_COMM_WORLD, &status);
		int cur_n = (status.MPI_SOURCE==(n_workers-1)) ? n_spicies_for_last:n_spicies_per_process; 
		std::vector <std::vector<bool> > subpopulation(2*cur_n);
		int offset=0;
		for(int i=0;i<subpopulation.size();++i){
			subpopulation[i].resize(n_dims);
			for(int j=0;j<subpopulation[i].size();++j)
				subpopulation[i][j] = msg_last[offset++]; 
			subpopulations[status.MPI_SOURCE-1] = subpopulation;
		}
	}
	delete msg;
	delete msg_last;
	
	#if debug_mode
	std::cout << "Selecting survivors" << std::endl;
	#endif
	
	//select best
	for (int i=0;i<population.size();++i){
		int best=0;
		for(int j=1;j<n_workers;++j)
			if((*F)(subpopulations[j][positions[j]])<(*F)(subpopulations[best][positions[best]]))
				best = j;
		population[i] = subpopulations[best][positions[best]++];	
	}
	
	#if debug_mode
	std::cout << "Quiting evolution_step" << std::endl;
	#endif
	
}	




/* ========================================================================= *
 * =              A SLAVE PROCESS TO PERFORM CROSSOVER, MUTATIONS AND SORT = *
 * ========================================================================= */

void crossover_and_sort_slave(double (*F)(std::vector<bool> &),int n_dims){
	
	int max_n = 2*n_dims*(default_population_size - (number_of_processes-2)*(default_population_size/(number_of_processes-1)) +2);
	#if debug_mode 
	std::cout<<"Worker process waiting for "<< max_n << " values max\n";
	#endif
	MPI_Status status;
	double *  buffer = new double[max_n];
	for(;;){
		// RECIVE DATA
		MPI_Recv(buffer, max_n, MPI_DOUBLE, 0, 0,
           MPI_COMM_WORLD, &status);
  
		#if debug_mode 
		std::cout<<"Worker process got data\n";
		#endif
			 
     // Last iteration => Shut down
     if(buffer[0]<0)
     	break;
     	
		std::vector <std::vector<bool> > subpopulation(buffer[1]);
		int offset=2;
		#pragma omp parallel for
		for(int i=0;i<subpopulation.size();++i){
			subpopulation[i].resize(buffer[0]);
			for(int j=0;j<subpopulation[i].size();++j)
				subpopulation[i][j] = buffer[offset++]; 
		}
		
		// CROSSOVER. Data is already randomly permuted so we can simply take 
		// two following
		
		std::vector<std::vector<bool> > new_generation = subpopulation;
		#pragma omp parallel for
		for (int i=1;i<new_generation.size();++i){
			//Crossower
			int p=rand()%new_generation[i].size();
			for(int l=0;l<p;++l)
				new_generation[i][l] = subpopulation[i-1][l];
			//MUTATIONS		
			for(int l=0;l<subpopulation[i].size();++l)
				if(!(rand()%100))
					new_generation[i][l] = !new_generation[i][l];
		}
		
		// Creating full population
		std::vector<std::vector<bool> > full_population = subpopulation;
		full_population.insert(full_population.end(), new_generation.begin(), 
																							new_generation.end());
		
		//Sorting (BAD SAD CODE)
		for(int i=0;i<full_population.size();++i)
			for(int j=i+1;j<full_population.size();++j)
				if((*F)(full_population[i]) > (*F)(full_population[j])){
					std::vector<bool> tmp = full_population[i];
					full_population[i] = full_population[j];
					full_population[j] = tmp;
				}
		
		#if debug_mode
		std::cout << "Best in this worker: "<<(*F)(full_population[0]) << '\n';
		#endif
		//Sending back
		#if debug_mode
		std::cout << "Sending "<< full_population.size()*full_population[0].size() 
						<< "values to master\n";
		#endif
		offset = 0;
		for(int i=0;i<full_population.size();++i)
			for(int j=0;j<full_population[i].size();++j)
				buffer[offset++] =full_population[i][j]; 
		
		MPI_Send(buffer, full_population.size()*full_population[0].size(), 
							MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
		
	}
	delete buffer;
}


/* ========================================================================= *
 * =                                                   GENETIC ALGROTHTHM  = *
 * ========================================================================= */
 
std::vector<bool> genetic_optimise(
	double (*F)(std::vector<bool> &),
	int n_dims,
	int population_size = default_population_size,
	double eps=0.01){
	
	int no_change = 0, max_no_change = 10; 
	double start_time = MPI_Wtime();
	
	std::vector< std::vector<bool> > population = 
		init_population(population_size, n_dims); // initialise_population
	
	double pre_min_value  = (*F)(population[0]), min_value=0., current_eps=0.;	
	do{
		
		evolution_step(F,population); // One time period
		
		min_value = (*F)(population[0]); //minimal value on current iteration
		current_eps = abs(min_value - pre_min_value); // change of optimal value
		pre_min_value = min_value; // required by next iteration
		std::cout << "\n*Current minimun: "<< min_value << "\nTime elapsed: "<< MPI_Wtime()-start_time <<"\n\n";
		if(current_eps < eps)
			no_change++;
		else
			no_change=0;
	} while(no_change<max_no_change); 
	
	//send all slave processes -1 to terminate them
	double end_code = -1;
	for(int i=1;i<number_of_processes;++i)
		MPI_Send(&end_code, 1, MPI_DOUBLE, i, 0, MPI_COMM_WORLD);
	
	
	return population[0]; 

}


/* ========================================================================= *
 * =                                                               MAIN()  = *
 * ========================================================================= */
 
int main(int argc, char **argv){
	
	// Initialisation of MPI
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &process_rank);
  MPI_Comm_size(MPI_COMM_WORLD, &number_of_processes);
  std::cout<<"running on "<<number_of_processes<<" processes\n";
  	if(!process_rank)
	#pragma omp parallel
		if(!omp_get_thread_num()){
			int n_threads = omp_get_num_threads();
			std::cout<<"threads number = "<<n_threads<<"\n";
			}
  if(number_of_processes<2){
  	if(!process_rank)
  		std::cerr<<"Cannot run master-slave on 1 thread"<<std::endl;
  	MPI_Finalize();
  	return EXIT_FAILURE;
  }
  
  srand (time(NULL));
  if(!process_rank){ // Master process
		genetic_optimise(loss,use_n_dims);
		
		std::cout << "OK\n";
	}else
		crossover_and_sort_slave(loss,use_n_dims);
	
	
  // Shut down MPI
  MPI_Finalize();
	
	return EXIT_SUCCESS;
}

