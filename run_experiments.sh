#!/bin/sh

echo "Weclome to SUPERPRAK experiments runner"
echo "2013 Maxim Novikov. Lomonosov MSU, CMC (BERAL Genetic Ver.)"
echo ""

H=`hostname`
if [ "$H" = "fen1" ]; then
	H=bluegene
fi

echo "Running on $H"


sleep 5

mkdir "results_$H" 2>/dev/null
cd "results_$H" 

# SET FILES LIST YOU WANT HERE
for c in genetic 
do
	mkdir $c 2>/dev/null
	cd $c
	rm *.out 2>/dev/null
	rm *.err 2>/dev/null
	
	if [ "$H" = "bluegene" ]; then
		mpixlcxx_r -qsmp=omp -o ./main ../../$c.cpp
		 if [ "$?" -ne "0" ]; then
		 	exit 1;
		 fi
		for i in 1 2 3 4 8 12 16 
		do
			mpisubmit.bg -n $i -w 00:10:00 -m smp ./main 	
		done
	fi
	cd ../
done

echo ''
echo ''
echo 'DONE'
echo "Submitted all jobs. Please wait for .out files to appear in 'results' folder"
echo ''


