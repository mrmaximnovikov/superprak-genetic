#!/bin/sh


module add intel/13.1.0   
module add openmpi/1.5.5-icc

sbatch  -n128 -t5 	ompi ./genetic
sbatch  -n64 -t5 	ompi ./genetic
sbatch  -n32 -t5 	ompi ./genetic
sbatch  -n16 -t5 	ompi ./genetic
sbatch  -n8  -t5 	ompi ./genetic
sbatch  -n4  -t5 	ompi ./genetic
sbatch  -n2  -t5 	ompi ./genetic
sbatch  -n64 -N32 -t5 ompi ./genetic
sbatch  -n64 -N16 -t5 ompi ./genetic
sbatch  -n64 -N8 -t5  ompi ./genetic
sbatch  -n32 -N16 -t5 ompi ./genetic
sbatch  -n32 -N8 -t5  ompi ./genetic
sbatch  -n32 -N4  -t5 ompi  ./genetic


